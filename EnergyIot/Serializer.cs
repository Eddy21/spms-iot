﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyIot
{
    public class Serializer
    {
        public string id { get; }
        public double energy_value { get; set; }
        public Serializer(string Id,double Energy_value)
        {
            this.id = Id;
            this.energy_value = Energy_value;
        }
    }
}
