﻿namespace EnergyIot
{
    partial class IoTForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.Container = new System.Windows.Forms.Panel();
            this.txtId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlEnergia = new System.Windows.Forms.Panel();
            this.txtEnergia = new System.Windows.Forms.TextBox();
            this.lblEnergia = new System.Windows.Forms.Label();
            this.lblKw = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Container.SuspendLayout();
            this.pnlEnergia.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Font = new System.Drawing.Font("Monotype Corsiva", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrar.Location = new System.Drawing.Point(485, 3);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(121, 44);
            this.btnRegistrar.TabIndex = 5;
            this.btnRegistrar.Text = "Generar";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // Container
            // 
            this.Container.AutoScroll = true;
            this.Container.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Container.Controls.Add(this.btnRegistrar);
            this.Container.Controls.Add(this.txtId);
            this.Container.Controls.Add(this.label1);
            this.Container.Controls.Add(this.pnlEnergia);
            this.Container.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Container.Location = new System.Drawing.Point(25, 12);
            this.Container.Name = "Container";
            this.Container.Size = new System.Drawing.Size(634, 228);
            this.Container.TabIndex = 3;
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(100, 13);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(366, 30);
            this.txtId.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Id";
            // 
            // pnlEnergia
            // 
            this.pnlEnergia.Controls.Add(this.checkBox1);
            this.pnlEnergia.Controls.Add(this.txtEnergia);
            this.pnlEnergia.Controls.Add(this.comboBox1);
            this.pnlEnergia.Controls.Add(this.label5);
            this.pnlEnergia.Controls.Add(this.lblEnergia);
            this.pnlEnergia.Controls.Add(this.lblKw);
            this.pnlEnergia.Location = new System.Drawing.Point(3, 49);
            this.pnlEnergia.Name = "pnlEnergia";
            this.pnlEnergia.Size = new System.Drawing.Size(562, 134);
            this.pnlEnergia.TabIndex = 14;
            this.pnlEnergia.Visible = false;
            // 
            // txtEnergia
            // 
            this.txtEnergia.Enabled = false;
            this.txtEnergia.Location = new System.Drawing.Point(97, 19);
            this.txtEnergia.Name = "txtEnergia";
            this.txtEnergia.Size = new System.Drawing.Size(366, 30);
            this.txtEnergia.TabIndex = 15;
            // 
            // lblEnergia
            // 
            this.lblEnergia.AutoSize = true;
            this.lblEnergia.Location = new System.Drawing.Point(4, 5);
            this.lblEnergia.Name = "lblEnergia";
            this.lblEnergia.Size = new System.Drawing.Size(65, 24);
            this.lblEnergia.TabIndex = 4;
            this.lblEnergia.Text = "Energia";
            // 
            // lblKw
            // 
            this.lblKw.AutoSize = true;
            this.lblKw.Location = new System.Drawing.Point(478, 19);
            this.lblKw.Name = "lblKw";
            this.lblKw.Size = new System.Drawing.Size(53, 24);
            this.lblKw.TabIndex = 2;
            this.lblKw.Text = "kW/h";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(97, 55);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(107, 28);
            this.checkBox1.TabIndex = 17;
            this.checkBox1.Text = "Encendido";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Proveedor",
            "Consumidor"});
            this.comboBox1.Location = new System.Drawing.Point(97, 89);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(366, 32);
            this.comboBox1.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 97);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 24);
            this.label5.TabIndex = 15;
            this.label5.Text = "Tipo";
            // 
            // IoTForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(671, 263);
            this.Controls.Add(this.Container);
            this.Name = "IoTForm";
            this.Text = "IoTForm";
            this.Load += new System.EventHandler(this.IoTForm_Load);
            this.Container.ResumeLayout(false);
            this.Container.PerformLayout();
            this.pnlEnergia.ResumeLayout(false);
            this.pnlEnergia.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.Panel Container;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlEnergia;
        private System.Windows.Forms.TextBox txtEnergia;
        private System.Windows.Forms.Label lblEnergia;
        private System.Windows.Forms.Label lblKw;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label5;
    }
}