﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EnergyIot
{
    public partial class IoTForm : Form
    {
        protected Comunicate comunicate;
        protected IoT iot;
        public IoTForm()
        {
            InitializeComponent();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            iot = new IoT(Guid.NewGuid().ToString());
            txtEnergia.Enabled = false;
            txtId.Text = iot.id;
            btnRegistrar.Visible = false;
            Thread oThread = new Thread(() =>
            {
                comunicate.registrar(iot).GetAwaiter().GetResult();
            });
            oThread.Start();
            oThread.Join();
            if (comunicate.status)
            {
                btnRegistrar.Visible = false;
                pnlEnergia.Visible = true;
            }
            else
            {
                MessageBox.Show("Error en logueo");
                btnRegistrar.Visible = true;
                txtId.Text = "";
                return;
            }
        }

        private void IoTForm_Load(object sender, EventArgs e)
        {
            comunicate = new Comunicate("https://g6yvxj7t4i.execute-api.eu-central-1.amazonaws.com/dev/iots");
            comboBox1.SelectedIndex = 0;
        }

        private void Send(Double value)
        {
            String tipo = comboBox1.SelectedItem.ToString();
            Thread oThread = new Thread(() =>
            {
                comunicate.update(tipo, iot, value).GetAwaiter().GetResult();
            });
            oThread.Start();
            oThread.IsBackground = true;
            if (!comunicate.status)
            {
                timer1.Enabled = false;
                MessageBox.Show("Error no se esta enviando correctamente");
                txtId.Enabled = true;
                btnRegistrar.Visible = true;
                pnlEnergia.Visible = false;
                txtId.Text = "";
                return;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Random rnd = new Random();
            double value = rnd.NextDouble() * (5000);
            double n = Math.Round(value, 2, MidpointRounding.AwayFromZero);
            txtEnergia.Text = n.ToString();
            Send(n);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                int value = 0;
                timer1.Interval = 2000;
                timer1.Enabled = true;
                comboBox1.Enabled = false;
            }
            else
            {
                timer1.Enabled = false;
                comboBox1.Enabled = true;
            }
        }
    }
}
