﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EnergyIot
{
    public class Comunicate
    {
        private HttpClient Client = new HttpClient();
        private string baseUrl;
        public Boolean status { get; set; }
        public Comunicate(String url)
        {
            Client.DefaultRequestHeaders.Add("Access-Control-Allow-Origin", "*");
            baseUrl = url;
        }

        public async Task registrar(IoT iot)
        {
            Console.WriteLine(iot.id);
            status = await Request($"{baseUrl}/{iot.id}", iot);
        }

        public async Task update(String tipo,IoT iot,Double energy)
        {
            if (tipo.Equals("Consumidor"))
            {
                energy = energy * -1;
            }
            Serializer obj = new Serializer(iot.id, energy);
            status = await Request($"{baseUrl}/{iot.id}", obj);
        }

        private async Task<Boolean> Request(string path, object data)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(data);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            try
            {
                response = await Client.PutAsync(path, content);
                Console.WriteLine(response.ToString());
                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
            catch
            {
                Console.WriteLine(response.Content);
                response.StatusCode = HttpStatusCode.BadRequest;
                return false;
            }
        }
    }
}
